FROM openjdk:8-jre-alpine
RUN wget -O /usr/local/lib/antlr-4.9-complete.jar https://www.antlr.org/download/antlr-4.9-complete.jar
COPY antlr4 /usr/local/bin
RUN apk add --no-cache dos2unix \
	&& dos2unix /usr/local/bin/antlr4 \
	&& apk del dos2unix
RUN chmod +x /usr/local/bin/antlr4
